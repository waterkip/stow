#!/usr/bin/perl
#
# This file is part of GNU Stow.
#
# GNU Stow is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GNU Stow is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see https://www.gnu.org/licenses/.

#
# Test case for dotfiles special processing
#

use strict;
use warnings;

use testutil;

use Test::More;
use English qw(-no_match_vars);

use testutil;

init_test_dirs();
cd("$TEST_DIR/target");

my $stow;

#
# process a dotfile marked with 'dot' prefix
#

$stow = new_Stow(dir => '../stow', dotfiles => 1);

make_path('../stow/dotfiles');
make_file('../stow/dotfiles/dot-foo');

$stow->plan_stow('dotfiles');
$stow->process_tasks();
is(
    readlink('.foo'),
    '../stow/dotfiles/dot-foo',
    => 'processed dotfile'
);

#
# ensure that turning off dotfile processing links files as usual
#

$stow = new_Stow(dir => '../stow', dotfiles => 0);

make_path('../stow/dotfiles');
make_file('../stow/dotfiles/dot-foo');

$stow->plan_stow('dotfiles');
$stow->process_tasks();
is(
    readlink('dot-foo'),
    '../stow/dotfiles/dot-foo',
    => 'unprocessed dotfile'
);


#
# process folder marked with 'dot' prefix
#

$stow = new_Stow(dir => '../stow', dotfiles => 1);

make_path('../stow/dotfiles/dot-emacs');
make_file('../stow/dotfiles/dot-emacs/init.el');

$stow->plan_stow('dotfiles');
$stow->process_tasks();
is(
    readlink('.emacs'),
    '../stow/dotfiles/dot-emacs',
    => 'processed dotfile folder'
);

#
# corner case: paths that have a part in them that's just "$DOT_PREFIX" or
# "$DOT_PREFIX." should not have that part expanded.
#

$stow = new_Stow(dir => '../stow', dotfiles => 1);

make_path('../stow/dotfiles');
make_file('../stow/dotfiles/dot-');

make_path('../stow/dotfiles/dot-.');
make_file('../stow/dotfiles/dot-./foo');

$stow->plan_stow('dotfiles');
$stow->process_tasks();
is(
    readlink('dot-'),
    '../stow/dotfiles/dot-',
    => 'processed dotfile'
);
is(
    readlink('dot-.'),
    '../stow/dotfiles/dot-.',
    => 'unprocessed dotfile'
);

#
# simple unstow scenario
#

$stow = new_Stow(dir => '../stow', dotfiles => 1);

make_path('../stow/dotfiles');
make_file('../stow/dotfiles/dot-bar');
make_link('.bar', '../stow/dotfiles/dot-bar');

$stow->plan_unstow('dotfiles');
$stow->process_tasks();
ok(
    $stow->get_conflict_count == 0 &&
    -f '../stow/dotfiles/dot-bar' && ! -e '.bar'
    => 'unstow a simple dotfile'
);


# Lets get into bug behaviour
# We try to mimic the situation wher you are in the stow dir and call bin/stow
use File::Temp;
use File::Spec::Functions qw(catfile rel2abs canonpath splitdir);
use POSIX qw(getcwd);
use Cwd qw(realpath);

# Setup things like in my homedir
my $target = File::Temp->newdir(dir =>'../stow');
my $home = rel2abs("$target");
$home = realpath( $home );

my $source = File::Temp->newdir(dir => $target);
my $dotty = rel2abs("$source");
$dotty = realpath( $dotty );

my $cwd = getcwd;
# Move into the stow dir
chdir($dotty);

# Create the setup
my $st = catfile($dotty, 'bug');
make_path("$st");
make_path("$st/dot-config");
make_file("$st/dot-config/some");

# The target directory must exists prior to stowing to expose the bug
make_path("$home/.config");

$stow = new_Stow(
    dir       => $dotty,
    target    => $home,
    dotfiles  => 1,
    verbose   => 4,
    test_mode => 0
);

is($stow->{dir}, $dotty, "Our stow dir is dotty");
is(
    $stow->{stow_path},
    (splitdir($dotty))[-1],
    "... and our relative path is also correct"
);

$stow->plan_stow('bug');
my %conflicts = $stow->get_conflicts;
fail("Has conflicts") if %conflicts;
diag explain $stow;

chdir($cwd);

done_testing;
